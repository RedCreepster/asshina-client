const express = require('express');
const app = express();

/**
 *
 * @param {Request} req
 * @param {Response} res
 */
function filtered(req, res) {
  let query = req.query;

  const source = query.source;

  /**
   * @var {Array}
   */
  try {
    let items = require(`./${source}.json`);

    if (query['filter']) {
      const filter = query.filter;
      const keys = Object.keys(filter);

      if (keys.length > 0) {
        items = items.filter(item => {
          let result = true;

          keys.forEach(key => {
            if (String(item[key]) !== String(filter[key])) {
              result = false;
            }
          });

          return result;
        });
      }
    }

    /**
     * @type number | null
     */
    const offset = Number(query.offset) || 0;
    /**
     * @type number | null
     */
    const limit = Number(query.limit) || 0;
    res.json(wrapItems(pagination(offset, limit, items), items.length));
  } catch (e) {
    res.json(wrapItems([], 0));
  }
}

/**
 *
 * @param {number} offset
 * @param {number} limit
 * @param {Array} items
 * @return Array
 */
function pagination(offset, limit, items) {
  return items
    .filter((item, index) => {
      return index > offset - 1;
    })
    .filter((item, index) => {
      return index < limit || limit === 0;
    });
}

/**
 * @param {Array} items
 * @param {number} totalCount
 * @returns {{items: Array, count: number}}
 */
function wrapItems(items, totalCount) {
  return {items: items, count: totalCount};
}

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/', filtered);

app.listen(3100);
