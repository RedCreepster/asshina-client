module.exports = {
  modules: [
    'bootstrap-vue/nuxt',
  ],
  head: {
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {hid: 'description', name: 'description', content: 'Meta description'},
    ],
    link: [
      {rel: 'icon', type: 'image/x-icon', href: 'favicon.ico'},
    ],
  },
  css: [
    '~/static/main.css',
    '~/static/fa/css/fontawesome.css',
    '~/static/fa/css/fa-brands.css',
    '~/static/fa/css/fa-solid.css',
  ],
  render: {
    bundleRenderer: {
      shouldPreload: (file, type) => ['script', 'style', 'font'].includes(type),
    },
  },
  build: {
    vendor: [
      '~/vue.config.js',
    ],
  },
};
