declare interface DTResponse<T> {
  count: number;
  items: Array<T>;
}
